﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstWeek
{
    static class Program
    {
        static string Küsimine(this string küsimus)
        {
            Console.Write(küsimus + ": ");
            return Console.ReadLine();
        }

        static void Main(string[] args)
        {
            // massiiv
            int[,] mass = new int[10, 10];
            // täidame arvudega
            Random r = new Random();
            //Console.WriteLine(r.Next(1,100));
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                    mass[i, j] = r.Next(1, 100);

            // proovime - seda ülesandes ei ole, aga endal hea vaadata
            for (int i = 0; i < 10; i++)
            {
                Console.Write($"rida {i}: ");
                for (int j = 0; j < 10; j++)
                    Console.Write($"{mass[i,j]} ");
                Console.WriteLine();
            }

            // hakkame otsima
            // võimalik for tsükliga
            // for(string loe = "*"; loe != "";)

            string loe = "*"; // kirjeldame muutuja NB! tal peab olema väärtus
            while (loe != "")  //mille järgi ma tean, et lõpetada
            {
                //Console.Write("anna üks arv: ");
                //loe = Console.ReadLine();

                loe = "anna üks arv".Küsimine();

                if (loe != "")
                {
                    
                    if (int.TryParse(loe, out int otsitav))
                    {
                        //Console.WriteLine("Hakkame otsima");
                        int lähimI = -1;
                        int lähimJ = -1;
                        int lähimVahe = int.MaxValue;

                        bool kasLeidsin = false;
                        for (int i = 0; i < 10; i++)
                            for (int j = 0; j < 10; j++)
                            {
                                if (mass[i,j] == otsitav)  // on otsitav
                                {
                                    Console.WriteLine($"leidsin arvu {otsitav} reast {i} veerust {j}");
                                    kasLeidsin = true;
                                }
                                else // ei ole otsitav
                                {
                                    int vahe = otsitav - mass[i, j];
                                    vahe = vahe < 0 ? -vahe : vahe;
                                    if (vahe < lähimVahe)
                                    {
                                        lähimVahe = vahe;
                                        lähimI = i;
                                        lähimJ = j;
                                    }

                                }

                            }
                        if (!kasLeidsin)
                        {
                            Console.WriteLine("sellist arvu ei leidnud");
                            Console.WriteLine($"lähim otsitavale on {mass[lähimI, lähimJ]} reast {lähimI} veerust {lähimJ}");

                        }
                    } // siin on loetud arv
                    else
                    {
                        Console.WriteLine("see pole miski arv, proovi uuesti");
                    } // siin on loetud miski muu

                }
            } 

        }
    }
}
